﻿#pragma strict

public var maxHealth : float;
private var currentHealth : float;
public var healthSlider : UI.Slider;
public var spawnPoint : Transform;
public var bloodPrefab : Transform;
public var Invincible: boolean;
public var InvincibleTime: float;

function Start(){
    currentHealth = maxHealth;
    Invincible=false;
}

function Update(){
	if(transform.position.y<-50){
	currentHealth = maxHealth;
			//healthSlider.value = 1;
			transform.position = spawnPoint.position;
	}
if (Time.time>InvincibleTime){
 	Invincible=false;
 	GetComponent(SpriteRenderer).color=Color.white;
 	}
}

function OnCollisionEnter2D (collision : Collision2D) {
	if (collision.gameObject.tag == "Enemy") {
		if (bloodPrefab != null) {
			Instantiate (bloodPrefab, collision.contacts[0].point, Quaternion.identity); 
		}
		if(! Invincible){

		currentHealth -= 6.9f;
		}
		if (currentHealth <= 0) {
			currentHealth = maxHealth;
			healthSlider.value = 1;
			transform.position = spawnPoint.position;
		} else {
			healthSlider.value = currentHealth / maxHealth;
		}
	}
	/*if (collision.gameObject.tag == "taco") {
		currentHealth += 6.9f;
		if (currentHealth >= maxHealth) {
			currentHealth = maxHealth;
			}
			healthSlider.value = currentHealth / maxHealth;
		}*/

}
public function healPlayer(ammount: float){
	currentHealth+=ammount; 
	if (currentHealth >= maxHealth) {
			currentHealth = maxHealth;
	}
	healthSlider.value = currentHealth / maxHealth;

}
public function makeInvincible (){
	Invincible=true;
	InvincibleTime=Time.time+5;
	GetComponent(SpriteRenderer).color=Color.red;

}
﻿#pragma strict

public var target : Transform;
public var smoothTime = 0.1f;
private var velocity = Vector3.zero;

function Start (){

}

function Update () {
    //transform.position.x = target.transform.position.x;
    //transform.position.y = target.transform.position.y;

    var updatedPosition = Vector3.SmoothDamp(transform.position, target.position, velocity, smoothTime);
    transform.position.x = updatedPosition.x;
    transform.position.y = updatedPosition.y;
}

public var maxHitpoints = 100.0f;
private var currentHitpoints : float;
public var collectablePrefabs : Transform[];

function Start () {
	currentHitpoints = maxHitpoints;
}

function TakeDamage(damage : float) {
    // Decrease hit points by damage
    currentHitpoints -= damage;

    // if hit points have reached zero
    if(currentHitpoints <= 0){
        var col = GetComponent(Collider2D);
        if (collectablePrefabs != null) {
            for (var prefab : Transform in collectablePrefabs) {
				if (prefab != null) {
                // Create a new instance of the prefab
                    Instantiate(prefab, transform.position + Vector3.Scale(col.bounds.size* 0.5f, Random.insideUnitCircle), Quaternion.identity);
                }
            }
        } 
        Destroy(gameObject);
    }
}